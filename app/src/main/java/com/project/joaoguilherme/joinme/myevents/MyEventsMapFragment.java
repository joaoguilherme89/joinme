package com.project.joaoguilherme.joinme.myevents;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.eventdialog.DialogEventInfo;
import com.project.joaoguilherme.joinme.eventdialog.EventDialogFragment;
import com.project.joaoguilherme.joinme.support.AllEventInfo;

import java.util.ArrayList;

public class MyEventsMapFragment extends Fragment
        implements EventDialogFragment.OnEventInfoSetListener {

    private MapView mapView;

    protected OnEventInfoReadyListener mEventsInfoListener;

    private String profileId;
    private String userName;

    private ArrayList<AllEventInfo> myEvents = new ArrayList<>();
    private AllEventInfo mapEvent;

    public static MyEventsMapFragment newInstance(Bundle allInfo) {

        // Consulta ao Banco de dados para carregar os eventos do usuário.
        MyEventsMapFragment myEventsMapFragment = new MyEventsMapFragment();
        myEventsMapFragment.setArguments(allInfo);

        return myEventsMapFragment;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_events_map, container, false);

        // User info....
        if (savedInstanceState == null) {
            profileId = getArguments().getString("PROFILE_ID");
            userName = getArguments().getString("USER_NAME");

            if (getArguments().getParcelableArrayList("MY_EVENTS") != null) {
                myEvents = getArguments().getParcelableArrayList("MY_EVENTS");
            }
        }

        mapView = (MapView) rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        // Animate MapView
        mapView.setAlpha(0f);
        mapView.animate().alpha(1f).setDuration(500);

        setHasOptionsMenu(true);

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //final Button addEventButton = (Button) rootView.findViewById(R.id.add_event);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                //googleMap.setMyLocationEnabled(true);

                addMarkersFromServer(googleMap, myEvents);

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {

                        // Unless the user clicks on the actionbar, add event label, mapEvent will be null.
                        if (mapEvent != null) {
                            // Set location for last element
                            mapEvent.setEventLocation(latLng);

                            // Adding new marker with default configuration...
                            MarkerOptions markerOpt = setMyDefaultMarkersConfig();
                            Marker marker = googleMap.addMarker(markerOpt.position(latLng));

                            // Add marker info
                            marker.setTitle(mapEvent.getEventHost());
                            marker.setSnippet(mapEvent.getEventName());

                            // Calls NotifyDatasetChange
                            mEventsInfoListener.onEventInfoReady(mapEvent);
                            mapEvent = null;
                        }
                    }
                });
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mEventsInfoListener = (OnEventInfoReadyListener) activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Restore state of list fragment
        if(savedInstanceState != null) {
            this.myEvents = savedInstanceState.getParcelableArrayList("MY_EVENTS");
            this.profileId = savedInstanceState.getString("PROFILE_ID");
            this.userName = savedInstanceState.getString("USER_NAME");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save current state of list fragment
        outState.putParcelableArrayList("MY_EVENTS", this.myEvents);
        outState.putString("PROFILE_ID", this.profileId);
        outState.putString("USER_NAME", this.userName);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {

        menuInflater.inflate(R.menu.menu_maps, menu);
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
                return true;

            case R.id.map_overlay:
                changeMapOverlay();
                return true;

            case R.id.add_event:
                addEvent();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addEvent() {

        if (mapView != null) {
            EventDialogFragment myDialog = EventDialogFragment.newInstance(profileId, userName);
            myDialog.show(getChildFragmentManager(), "EVENT_DIALOG_FRAGMENT");
        }
    }

    private void changeMapOverlay() {
        // Alert and change mapView overlay....
    }

    private void openSettings() {

    }

    /**
     * Set Default Configuration of user's markers.
     * @return markerOptions
     */
    private MarkerOptions setMyDefaultMarkersConfig () {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));

        return markerOptions;
    }

    public void showInstructionDialog () {
        new AlertDialog.Builder(getActivity())
                .setMessage("Click on the map to set location for event")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * Add markers for the events loaded from the Bundle
     * @param googleMap
     * @param events
     */
    private void addMarkersFromServer (GoogleMap googleMap, ArrayList<AllEventInfo> events) {

        MarkerOptions markerOpt = setMyDefaultMarkersConfig();

        for (AllEventInfo event : events) {
            if (event.getEventLocation() != null) {
                Marker marker = googleMap.addMarker(markerOpt.position(event.getEventLocation()));
                marker.setSnippet(event.getEventName());
                marker.setTitle(event.getEventHost());
            }
        }
    }

    /**
     * Return of Dialog Box!!!
     * @param dialogEventInfo
     */
    @Override
    public void onEventInfoSet(DialogEventInfo dialogEventInfo) {
        if (dialogEventInfo != null) {
            mapEvent = new AllEventInfo(dialogEventInfo, profileId, userName);
        }
    }

    public interface OnEventInfoReadyListener {
        public void onEventInfoReady(AllEventInfo allEventInfo);
    }
}
