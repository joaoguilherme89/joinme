package com.project.joaoguilherme.joinme.eventdialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.facebook.login.widget.ProfilePictureView;
import com.project.joaoguilherme.joinme.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventDialogFragment extends DialogFragment {

    private final static int EVENT_DIALOG_CODE = 0;
    public final static int TIME_OK = 1;
    public final static int DATE_OK = 2;

    protected OnEventInfoSetListener mEventInfoSet;

    private String date = null;
    private String time = null;
    private String id = null;
    private String eventName = null;
    private String shortDescription = null;
    private String userName = null;


    public static EventDialogFragment newInstance(String profileId, String userName) {

        Bundle profileInfo = new Bundle();
        profileInfo.putString("PROFILE_ID", profileId);
        profileInfo.putString("USER_NAME", userName);

        EventDialogFragment eventDialogFragment = new EventDialogFragment();
        eventDialogFragment.setArguments(profileInfo);

        return eventDialogFragment;
    }

    public EventDialogFragment() {
        // Required empty public constructor
    }

    public void onAttachFragment (Fragment fragment) {
        try {
            mEventInfoSet = (OnEventInfoSetListener) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()+" must implement OnEventInfoSetListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Attach caller fragment to DialogFragment....
        onAttachFragment(getParentFragment());

        final Bundle profileInfo = getArguments();
        View view = inflater.inflate(R.layout.fragment_event_dialog, container, false);

        id = profileInfo.getString("PROFILE_ID");
        ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profile_fragment);
        profilePictureView.setProfileId(id);

        // Get userName from caller fragment.
        userName = profileInfo.getString("USER_NAME");

//        final LatLng latLng = profileInfo.getParcelable("LOCATION");

        final EditText event = (EditText) view.findViewById(R.id.event_name);
        final EditText description = (EditText) view.findViewById(R.id.short_description);


        // Display time Dialog...
        ImageButton timeBtn = (ImageButton) view.findViewById(R.id.time_fragment);
        timeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

        // Display date Dialog...
        ImageButton dateBtn = (ImageButton) view.findViewById(R.id.calendar_fragment);
        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        // Validate Dialog...
        Button ok = (Button) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventName = event.getText().toString();
                shortDescription = description.getText().toString();

                DialogEventInfo dialogEventInfo =
                        new DialogEventInfo(id, eventName, shortDescription, time, date);

                // Check which fields have been filled..
                if (dialogEventInfo.existAnyNullField()) {
                    // Call dialog for warning
                    showAlertDialog ();
                } else {
                    // Retrieve data and state of add marker to MyEventsMapFragment
                    mEventInfoSet.onEventInfoSet(dialogEventInfo);
                    getDialog().dismiss();
                }
            }
        });

        // Cancel Dialog...
        Button cancel = (Button) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEventInfoSet.onEventInfoSet(null);
                getDialog().dismiss();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EVENT_DIALOG_CODE && resultCode == TIME_OK) {
            time = data.getStringExtra("TIME_CHOSEN");
        }
        if (requestCode == EVENT_DIALOG_CODE && resultCode == DATE_OK) {
            date = data.getStringExtra("DATE_CHOSEN");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle("New Event");

        return dialog;
    }

    /**
     * Warning for user to provide all event info
     */
    public void showAlertDialog () {
        new AlertDialog.Builder(getActivity())
                .setMessage("Fill All Event Info")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mEventInfoSet = null;
    }

    /**
     * Calls Time Picker Dialog Fragment from Event Dialog
     */
    public void showTimePickerDialog() {
        DialogFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.setTargetFragment(this, EVENT_DIALOG_CODE);
        timePickerFragment.show(getChildFragmentManager(), "timePicker");
    }

    /**
     * Calls Date Picker Dialog Fragment from Event Dialog
     */
    public void showDatePickerDialog() {
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setTargetFragment(this, EVENT_DIALOG_CODE);
        datePickerFragment.show(getChildFragmentManager(), "datePicker");
    }

    public interface OnEventInfoSetListener {
        void onEventInfoSet (DialogEventInfo dialogEventInfo);
    }
}
