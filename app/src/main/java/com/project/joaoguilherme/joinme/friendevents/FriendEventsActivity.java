package com.project.joaoguilherme.joinme.friendevents;

import android.app.AlertDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.eventdialog.DialogEventInfo;
import com.project.joaoguilherme.joinme.eventdialog.EventDialogFragment;
import com.project.joaoguilherme.joinme.myevents.MyEventFragmentAdapter;

import java.util.ArrayList;

import static com.google.android.gms.maps.GoogleMap.OnMapClickListener;


/**
 * Controls map interaction with tab
 *
 * author: João Guilherme Mattos
 */
public class FriendEventsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private SupportMapFragment mapFragment;

    // Keeps track of my events
    private ArrayList<DialogEventInfo> myEventsInfoList;

    // Returns state of dialog fragment
    private boolean dialogOk = false;
    private Bundle profileAssetsBundle;

    private LatLng lastLocation;

    private GoogleApiClient mGoogleApiClient;

    // Sets the code for attaching the map and events to the activity
    private ViewPager mViewPager;
    private MyEventFragmentAdapter myEventFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friend_events);

        Intent intent = getIntent();
        profileAssetsBundle = intent.getBundleExtra("PROFILE_ASSETS");

//        mViewPager = (ViewPager) findViewById(R.id.pager);
//        myEventFragments = new MyEventFragmentAdapter(getSupportFragmentManager());
//        mViewPager.setAdapter(myEventFragments);

//        FragmentManager fragManager = getSupportFragmentManager();
//        FragmentTransaction fragTransaction = fragManager.beginTransaction();
//
//        // Initializing new map default fragment
//        if (savedInstanceState == null) {
//            mapFragment = SupportMapFragment.newInstance(setDefaultMapOptions());
//            fragTransaction.add(R.id.map, mapFragment).commit();
//
//        } else {
//            mapFragment = (SupportMapFragment) fragManager.findFragmentById(R.id.map);
//        }
//        mMap = mapFragment.getMap();
//        mapFragment.getMapAsync(this);
//
//        buildGoogleApiClient();
    }


    // Get user location through LocationServices API
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {

                        // Get current location of user
                        Location myLocation = new Location(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
                        lastLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        new AlertDialog.Builder(FriendEventsActivity.this)
                                .setTitle(R.string.cancel)
                                .setMessage(R.string.location_issue)
                                .setPositiveButton(R.string.ok, null)
                                .show();

                        lastLocation = null;
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        new AlertDialog.Builder(FriendEventsActivity.this)
                                .setTitle(R.string.cancel)
                                .setMessage(R.string.location_issue)
                                .setPositiveButton(R.string.ok, null)
                                .show();

                        lastLocation = null;
                    }
                })
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Default Map configurations
     * @return gMapOptions
     */
    private GoogleMapOptions setDefaultMapOptions () {

        GoogleMapOptions gMapOptions = new GoogleMapOptions();
        gMapOptions.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(true)
                .rotateGesturesEnabled(true)
                .tiltGesturesEnabled(true)
                .scrollGesturesEnabled(true)
                .zoomGesturesEnabled(true)
                .zOrderOnTop(false);

        return gMapOptions;
    }

    /**
     * Set Default Configuration of user's markers.
     * @return markerOptions
     */
    private MarkerOptions setMyDefaultMarkersConfig () {

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        return markerOptions;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        googleMap.setMyLocationEnabled(true);

        if (lastLocation != null) {
            // Set zoom when activity starts
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, 40));
        }

        //----------------------------------------------------------------------------------------
        // Implement Button that changes map layers
        // button.showMapTypeDialog
        // Add this view

        // Adding markers to the map
        googleMap.setOnMapClickListener(new OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // ADICIONAR A ESSE DIALOG A LOCALIZAÇÃO DO EVENTO
                showNewEventDialog(latLng);

                // Set marker config
                MarkerOptions mOptions = setMyDefaultMarkersConfig();
                if (dialogOk) {
                    // Sets dialog to false so no other balloon is created.
                    dialogOk = false;

                    // Add title to marker
                    googleMap.addMarker(mOptions.position(latLng)
                            // Trocar titulo para nome do host
                            .title(myEventsInfoList.get(myEventsInfoList.size() - 1).getEventName())
                            .snippet(myEventsInfoList.get(myEventsInfoList.size() - 1).getEventDescription()));

                }
            }
        });
    }

    /**
     * Shows new event dialog
     *
     * author; João Guilherme Mattos
     */
    public void showNewEventDialog(LatLng latLng) {

        // Instantiates map tab fragment - Nao precisa estar declarado como variavel da Activity por enquanto
        EventDialogFragment eventDialogFragment =
                EventDialogFragment.newInstance(profileAssetsBundle.getString("PROFILE_ID"),
                        profileAssetsBundle.getString("USER_FIRST_NAME"));

        // On map ready, add map tab!
        eventDialogFragment.show(getSupportFragmentManager(), "Dialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_maps, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
            case R.id.action_search:
                openSearch();
        }

        return super.onOptionsItemSelected(item);
    }

    private void openSettings() {

    }

    private void openSearch() {

    }
}
