package com.project.joaoguilherme.joinme.myevents;

import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.support.AllEventInfo;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MyEventDetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_event_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        AllEventInfo event = intent.getParcelableExtra("EVENT");

        // Populating view with data from ArrayList
        ProfilePictureView profile = (ProfilePictureView) findViewById(R.id.profile);
        profile.setProfileId(event.getProfileId());

        TextView date = (TextView) findViewById(R.id.date);
        date.setText(event.getEventDate());

        TextView time = (TextView) findViewById(R.id.time);
        time.setText(event.getEventTime());

        TextView going = (TextView) findViewById(R.id.going);
        going.setText("0 going");

        TextView description = (TextView) findViewById(R.id.description);
        description.setText(event.getEventDescription());

        // Gets location from LatLng object
        TextView location = (TextView) findViewById(R.id.location);
        try {
            Geocoder geocoder = new Geocoder(MyEventDetailsActivity.this, Locale.US);
            List<Address> address = geocoder.getFromLocation(event.getEventLocation().latitude, event.getEventLocation().longitude, 1);
            location.setText(address.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case R.id.add_alarm:
                showAlarmDialog();
                return true;
            case R.id.invite_friends:
                showListOfFriends();
                return true;
            case R.id.edit_event:
                showEditEventDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAlarmDialog() {

    }

    private void showListOfFriends() {

    }

    private void showEditEventDialog() {

    }
}
