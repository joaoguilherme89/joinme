package com.project.joaoguilherme.joinme.appstart;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.friendevents.FriendEventsActivity;
import com.project.joaoguilherme.joinme.myevents.MyEventsActivity;

import java.util.Arrays;

public class StartMenuActivity extends ActionBarActivity {

    private CallbackManager callbackManager;

    private LinearLayout loggedInLayout;
    private LinearLayout loggedOutLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(StartMenuActivity.this);
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_start_menu);

        loggedInLayout = (LinearLayout) findViewById(R.id.logged_in);
        loggedOutLayout = (LinearLayout) findViewById(R.id.logged_out);

        if(isUserLoggedIn()) {

//            getSupportActionBar().show();

            loggedInLayout.setVisibility(View.VISIBLE);
            loggedOutLayout.setVisibility(View.GONE);

            // Get Profile Assets ..
            Profile profile = Profile.getCurrentProfile();

            // Get List of friends ..

            // Populate views
            TextView totalFriends = (TextView) findViewById(R.id.friends);
            totalFriends.setText("0 Friends");

            ProfilePictureView fbProfilePic = (ProfilePictureView) findViewById(R.id.profile);
            fbProfilePic.setProfileId(profile.getId());

            // Add UserName and ProfileId to Bundle for other Activities
            final Bundle profileInfo = new Bundle();
            profileInfo.putString("USER_FIRST_NAME", profile.getFirstName());
            profileInfo.putString("PROFILE_ID", profile.getId());

            // Set buttons listeners to start other activities
            Button friendEventBtn = (Button) findViewById(R.id.friend_events);
            friendEventBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StartMenuActivity.this, FriendEventsActivity.class);
                    intent.putExtra("PROFILE_ASSETS", profileInfo);
                    startActivity(intent);
                }
            });

            Button myEventsBtn = (Button) findViewById(R.id.my_events);
            myEventsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StartMenuActivity.this, MyEventsActivity.class);
                    intent.putExtra("PROFILE_ASSETS", profileInfo);
                    startActivity(intent);
                }
            });

        } else {
            // Elements from LoggedOutLayout
//            getSupportActionBar().hide();

            LoginButton logInButton = (LoginButton) findViewById(R.id.login_button);
            logInButton.setReadPermissions(Arrays.asList("email", "user_friends"));
            logInButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

//                    getSupportActionBar().show();

                    // Switch layout visibility
                    loggedOutLayout.setVisibility(View.GONE);
                    loggedInLayout.setVisibility(View.VISIBLE);

                    // Elements from LoggedInLayout
                    TextView totalFriends = (TextView) findViewById(R.id.friends);
                    ProfilePictureView fbProfilePic = (ProfilePictureView) findViewById(R.id.profile);

                    // Learn How to get Friends list.
                    totalFriends.setText("0 Friends");

                    // Get current user profile
                    Profile profile = Profile.getCurrentProfile();
                    fbProfilePic.setProfileId(profile.getId());

                    final Bundle profileInfo = new Bundle();
                    profileInfo.putString("USER_FIRST_NAME", profile.getFirstName());
                    profileInfo.putString("PROFILE_ID", profile.getId());

                    Button friendEventBtn = (Button) findViewById(R.id.friend_events);
                    friendEventBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(StartMenuActivity.this, FriendEventsActivity.class);
                            intent.putExtra("PROFILE_ASSETS", profileInfo);
                            startActivity(intent);
                        }
                    });

                    Button myEventsBtn = (Button) findViewById(R.id.my_events);
                    myEventsBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(StartMenuActivity.this, MyEventsActivity.class);
                            intent.putExtra("PROFILE_ASSETS", profileInfo);
                            startActivity(intent);
                        }
                    });
                }
                @Override
                public void onCancel() {
                    showAlert();
                }

                @Override
                public void onError(FacebookException e) {
                    showAlert();
                }

                private void showAlert() {
                    new AlertDialog.Builder(StartMenuActivity.this)
                            .setTitle(R.string.cancel)
                            .setMessage(R.string.login_failed)
                            .setPositiveButton(R.string.ok, null)
                            .show();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(this);
    }
    @Override
    protected void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.activateApp(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
                return true;

            case R.id.log_out:
                logOutFromApp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Implement this method to display switch user and log out functionalities.
     *
     * author: João Guilherme Mattos
     */
    private void openSettings() {

    }

    /**
     * Check if all permissions have been accepted by the user
     * @return boolean
     */
    private boolean checkPermission () {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        return accessToken.getPermissions().containsAll(Arrays.asList("email", "user_friends"));

    }


    // Finish later
    private void getUserFriends () {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken.getPermissions().contains(Arrays.asList("user_friends")) == true) {
            // Get user friends list
        }
    }

    /**
     * Check if facebook is logged in
     */
    private boolean isUserLoggedIn () {
        return AccessToken.getCurrentAccessToken() != null;
    }

    /**
     * Logs out from JoinMe..
     */
    private void logOutFromApp() {
        loggedInLayout.setVisibility(View.GONE);
        loggedOutLayout.setVisibility(View.VISIBLE);

        //Sets accessToken to null and restarts activity
        AccessToken.setCurrentAccessToken(null);
        StartMenuActivity.this.onRestart();
    }
}
