package com.project.joaoguilherme.joinme.eventdialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;


public class DatePickerFragment extends DialogFragment
                        implements DatePickerDialog.OnDateSetListener {

    public static DatePickerFragment newInstance() {
        DatePickerFragment fragment = new DatePickerFragment();
        return fragment;
    }

    public DatePickerFragment() {
        // Required empty public constructor
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog (getActivity(), this, day, month, year);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        String y = Integer.toString(year);
        String m = Integer.toString(monthOfYear);
        String d = Integer.toString(dayOfMonth);

        String date = d + "/" + m + "/" + y;

        // Set data to be retrieved to EventDialogFragment
        Intent intent = new Intent();
        intent.putExtra("DATE_CHOSEN", date);
        getTargetFragment().onActivityResult(getTargetRequestCode(), EventDialogFragment.DATE_OK, intent);
    }
}
