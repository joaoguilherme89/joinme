package com.project.joaoguilherme.joinme.support;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Created by Joao Guilherme on 6/04/2015.
 */
public class BackgroundTask extends AsyncTask<Void, Void, Void> {

    private ProgressDialog progressDialog;
    private long time;

    public BackgroundTask (Activity activity, long time) {
        this.time = time;
        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("Wait while loading data");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            Thread.sleep(this.time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
