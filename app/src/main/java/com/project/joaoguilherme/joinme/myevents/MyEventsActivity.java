package com.project.joaoguilherme.joinme.myevents;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.support.AllEventInfo;

import java.util.ArrayList;


// UNICA CLASSE QUE INTERAGE COM O SERVIDOR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


public class MyEventsActivity extends ActionBarActivity implements
            MyEventsMapFragment.OnEventInfoReadyListener {

        private GoogleMap mMap; // Might be null if Google Play services APK is not available.
        private SupportMapFragment mapFragment;

        // Keeps track of my events
        private ArrayList<AllEventInfo> myEvents = new ArrayList<>();

        // All user info and events..
        private Bundle allInfo;

        // Sets the code for attaching the map and events to the activity
        private ViewPager mViewPager;
        private PagerTabStrip mPagerTabStrip;
        private MyEventFragmentAdapter myEventFragments;

        private ArrayList<Fragment> mFragments;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_my_events);

            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);

            // Defines animation for loading Pager in Activity
            View pager = findViewById(R.id.layout);
            pager.setVisibility(View.GONE);

            View loading = findViewById(R.id.loading_spinner);
            crossFading(pager, loading);

            Intent intent = getIntent();
            Bundle profileAssetsBundle = intent.getBundleExtra("PROFILE_ASSETS");

            String profileId = profileAssetsBundle.getString("PROFILE_ID");
            String userName = profileAssetsBundle.getString("USER_FIRST_NAME");

            if (savedInstanceState == null) {

                // GET ALL EVENTS FROM THE SERVER TO MYEVENTS
                AllEventInfo event = new AllEventInfo(profileId, userName, "Aniversario",
                        "Casa do seu ze", "11:30", "11/05/2014", new LatLng(-33.796923, 150.922433));

                myEvents.add(event);

                // Configure Bundle to FragmentAdapter!!
                allInfo = new Bundle();
                allInfo.putString("PROFILE_ID", profileId);
                allInfo.putString("USER_NAME", userName);

                if (myEvents.size() > 0) {
                    allInfo.putParcelableArrayList("MY_EVENTS", this.myEvents);
                }
            }

            mFragments = new ArrayList<>();
            mFragments.add(MyEventsMapFragment.newInstance(allInfo));
            mFragments.add(MyEventsListFragment.newInstance(allInfo));

            // Configuring layout of the PagerTabStrip
            mPagerTabStrip = (PagerTabStrip) findViewById(R.id.pager_tab_strip);
            mPagerTabStrip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);

            // Set adapter for initializing fragments.
            mViewPager = (ViewPager) findViewById(R.id.pager);
            myEventFragments = new MyEventFragmentAdapter(getSupportFragmentManager(), MyEventsActivity.this, this.mFragments);
            mViewPager.setAdapter(myEventFragments);
        }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("MY_EVENTS", this.myEvents);
        outState.putBundle("ALL_INFO", this.allInfo);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.myEvents = savedInstanceState.getParcelableArrayList("MY_EVENTS");
        this.allInfo = savedInstanceState.getBundle("ALL_INFO");

        super.onRestoreInstanceState(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void crossFading(View fadeIn, final View fadeOut) {
        fadeIn.setAlpha(0f);
        fadeIn.setVisibility(View.VISIBLE);

        fadeIn.animate().alpha(1f).setDuration(2000).setListener(null);

        fadeOut.animate().alpha(0f).setDuration(2000).setListener(new AnimatorListenerAdapter (){
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                fadeOut.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onEventInfoReady(AllEventInfo allEventInfo) {
        if (allEventInfo != null) {
            myEvents.add(allEventInfo);

            allInfo.putParcelableArrayList("MY_EVENTS", this.myEvents);
            // Notifies adapter that arraylist has changed
//            myEventFragments.notifyDataSetChanged();
        }
    }
}
