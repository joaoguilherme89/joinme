package com.project.joaoguilherme.joinme.eventdialog;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment
                            implements TimePickerDialog.OnTimeSetListener {

    public static TimePickerFragment newInstance() {
        return new TimePickerFragment();
    }

    public TimePickerFragment() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final Calendar calendar = Calendar.getInstance();

        int hour = Calendar.HOUR_OF_DAY;
        int minute = Calendar.MINUTE;

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String time = getTimeSet(hourOfDay, minute).toString();

        Toast.makeText(getActivity(), "Time: " + time, Toast.LENGTH_LONG).show();

        // Set data to be retrieved to EventDialogFragment
        Intent intent = new Intent();
        intent.putExtra("TIME_CHOSEN", time);
        getTargetFragment().onActivityResult(getTargetRequestCode(), EventDialogFragment.TIME_OK, intent);
    }

    /**
     * Sets time chosen by the user..
     *
     * @param hour
     * @param minute
     * @return
     */
    private StringBuilder getTimeSet(Integer hour, Integer minute) {
        if (minute < 10) {
            return new StringBuilder().append(hour.toString()).append(":0").append(minute.toString());
        }
        return new StringBuilder().append(hour.toString()).append(":").append(minute.toString());
    }
}
