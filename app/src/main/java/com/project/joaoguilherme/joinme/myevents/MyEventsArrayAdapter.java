package com.project.joaoguilherme.joinme.myevents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.project.joaoguilherme.joinme.R;
import com.project.joaoguilherme.joinme.support.AllEventInfo;

import java.util.ArrayList;

/**
 * Sets custom view to be the adapter to the fragment list.
 *
 * Created by Joao Guilherme on 14/04/2015.
 */
public class MyEventsArrayAdapter extends ArrayAdapter<AllEventInfo> {

    private final Context context;
    private final ArrayList<AllEventInfo> rowEvent;

    public MyEventsArrayAdapter(Context context, ArrayList<AllEventInfo> events) {
        super(context, R.layout.event_item, events);
        this.context = context;
        this.rowEvent = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Inflating layout -> LAYOUT_INFLATER_SERVICE enables this context to inflate layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.event_item, parent, false);

        // Initialize the view holder
        ViewHolder viewHolder = new ViewHolder();

        // Populating row view with data from ArrayList
        viewHolder.profile = (ProfilePictureView) row.findViewById(R.id.profile);
        viewHolder.event = (TextView) row.findViewById(R.id.event);
        viewHolder.date = (TextView) row.findViewById(R.id.event_date);
        viewHolder.time = (TextView) row.findViewById(R.id.event_time);
        row.setTag(viewHolder);

        // Get event info related to passed position
        AllEventInfo eventInfo = getItem(position);

        viewHolder.profile.setProfileId(eventInfo.getProfileId());
        viewHolder.event.setText(eventInfo.getEventName());
        viewHolder.date.setText(eventInfo.getEventDate());
        viewHolder.time.setText(eventInfo.getEventTime());

        return row;
    }

    @Override
    public int getCount() {
        if (this.rowEvent == null) {
            return 0;
        }
        return this.rowEvent.size();
    }

    private static class ViewHolder {
        ProfilePictureView profile;
        TextView event;
        TextView time;
        TextView date;
    }
}
